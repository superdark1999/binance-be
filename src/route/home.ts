import * as express from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { checkJwt } from "../middlewares/checkJwt";
import hash from "../utils/hashPassword";
import userModel from "../db/connection";

const router = express.Router();

router.get("/invitecode", checkJwt, async (req, res) => {
  const token = <string>req.headers["auth"];

  const user = await getUserByToken(token);
  res.send(user.inviteCode);
});

router.get("/profile", checkJwt, async (req, res) => {
  const token = <string>req.headers["auth"];

  const user = await getUserByToken(token);

  const result = await userModel.getTotal(user);
  res.send(result);
});

async function getUserByToken(token) {
  //Try to validate the token and get data
  try {
    var { payload } = jwt.decode(token, { complete: true });

    return await userModel.getUserByEmail(payload.email);
  } catch (error) {
    return;
  }
}

export default router;
