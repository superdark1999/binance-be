import * as express from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { checkJwt } from "../middlewares/checkJwt";
import hash from "../utils/hashPassword";
import userModel from "../db/connection";

const router = express.Router();

router.get("/", async (req, res) => {});

router.get("/users", checkJwt, async (req, res) => {
  const users = await userModel.getAllUser();
  res.send(users);
});

router.post("/login", async (req, res) => {
  const { email, password, addressWallet } = req.body;

  // get user from api
  const user = await userModel.getUserByEmail(email);

  //validate
  if (!user) return res.status(400).send("Invalid Email");
  if (!hash.comparePassword(password, user.password))
    return res.status(400).send("Invalid Password");

  // jwt sign to get token
  const token = jwt.sign({ email: user.email }, config.jwtSecret, {
    expiresIn: "1h",
  });

  // empty invite code
  const finalAddressWallet = addressWallet === "" ? "FS00000" : addressWallet;
  // Invalid invite code

  // Valid invite code
  if (!user.parentId && user.email !== "admin@gmail.com") {
    const parent = await userModel.getUserByAddressWallet(finalAddressWallet);
    if (parent) {
      user.parentId = parent._id;
      await user.save();

      await userModel.getListReward(finalAddressWallet, 3, 100);

      parent.childrenId.push(user._id);
      await parent.save();
    }
  }

  //Send the jwt in the response
  return res.send(token);
});

router.post("/register", async (req, res) => {
  // console.log(req.body);
  // validate user
  const { email, password, confirmPassword } = req.body;

  if (!email || !password || !confirmPassword)
    return res.status(400).send("Bad request");
  if (password !== confirmPassword)
    return res.status(400).send("Password isn't same confirmPassword");
  const user = await userModel.getUserByEmail(email);
  if (user) return res.status(400).send("Duplicate Email");

  const addressWallet =
    req.body.addressWallet !== "" ? req.body.addressWallet : "FS00000";

  // const token = <string>req.headers["auth"];

  // const user = await getUserByToken(token);
  await userModel.getListReward(addressWallet, 3, 100);
  await userModel.createUser(req.body, addressWallet);

  res.send("Successful");
});

export default router;
