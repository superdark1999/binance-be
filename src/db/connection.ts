const mongoose = require("mongoose");
import hash from "../utils/hashPassword";

mongoose
  .connect("mongodb://localhost/invite-app", {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => console.log("Connected to Mongodb......."))
  .catch(() => console.error("Could not connect to mongodb"));

const userSchema = mongoose.Schema({
  email: {
    type: String,
    unique: true,
  },
  password: String,
  addressWallet: String,
  parentId: String,
  point: Number,
  bonus: {
    type: Number,
    max: 1000,
  },
  childrenId: Array,
});

const User = mongoose.model("User", userSchema);

async function createUser(newUser, addressWallet) {
  const parentUser = await getUserByAddressWallet(addressWallet);
  const user = new User({
    email: newUser.email,
    password: hash.hashPassword(newUser.password),
    addressWallet: "FS" + Math.random().toString(36).substring(7),
    bonus: 0,
    point: 100,
    parentId: parentUser._id,
    childrenId: [],
  });

  //Init admin data
  // const user = new User({
  //   email: newUser.email,
  //   password: hash.hashPassword(newUser.password),
  //   addressWallet: "FS00000",
  //   parentId: null,
  //   childrendId: [],
  // });
  try {
    const result = await user.save();

    parentUser.childrenId.push(result._id);
    await parentUser.save();
  } catch (ex) {
    console.log(ex);
  }
}

async function getAllUser() {
  return await User.find();
}

async function getUserByEmail(email) {
  const user = await User.findOne({ email: email });
  return user;
}

async function getUserByAddressWallet(addressWallet) {
  const user = await User.findOne({ addressWallet: addressWallet });
  return user;
}

async function getListReward(addressWallet, numberOfReward, reward) {
  let user = await getUserByAddressWallet(addressWallet);
  if (!user) return;

  for (let i = 0; i < numberOfReward; i++) {
    if (user.bonus <= 1000) {
      let bonus = reward * (((numberOfReward - i) * 10) / 100);
      if (user.bonus + bonus > 1000) {
        bonus = 1000 - user.bonus;
      }
      user.bonus += bonus;
      user.point += bonus;
    }

    await user.save();

    console.log(
      `user: ${user.email}, wallet: ${user.point}, bonus: ${user.bonus}`
    );

    //get next parent
    user = await User.findById(user.parentId);

    if (!user) break;
  }
}

async function getTotal(user) {
  if (!user) return [];

  // f0
  let result = [];
  let f0 = await getChildrenOfUser(user);
  if (!f0) return [];
  result.push(f0.length);

  for (let i = 0; i < 2; i++) {
    let f1 = [];
    for (let i = 0; i < f0.length; i++) {
      const childUser = await getChildrenOfUser(f0[i]);
      f1 = f1.concat(childUser);
    }
    result.push(f1.length);
    f0 = [...f1];
  }

  return result;
}

async function getChildrenOfUser(user) {
  let listChildren = [];

  if (!user) return listChildren;

  for (let i = 0; i < user.childrenId.length; i++) {
    const childUser = await User.findById(user.childrenId[i]);
    listChildren.push(childUser);
  }

  return listChildren;
}

export default {
  createUser,
  getAllUser,
  getUserByEmail,
  getListReward,
  getTotal,
  getUserByAddressWallet,
};
