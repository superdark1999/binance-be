const bcrypt = require("bcryptjs");

const salt = bcrypt.genSaltSync(10);

export default {
  hashPassword(password) {
    const hash = bcrypt.hashSync(password, salt);
    return hash;
  },

  comparePassword(password, comparePassword) {
    return bcrypt.compareSync(password, comparePassword);
  },
};
