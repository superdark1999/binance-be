import * as express from "express";
import * as cors from "cors";
import auth from "./route/auth";
import home from "./route/home";

const app = express();

app.use(express.json());

app.use(cors());

app.use("/auth", auth);

app.use("/", home);

app.listen(5000);
